part of evergage;

/// Provides APIs to handle data campaigns, track Item views and interactions, and track manual actions, all within a lifecycle-managed context such as Screen.
class EvergageContext {
  final MethodChannel _channel;
  bool _simulation = false;

  EvergageContext._(MethodChannel channel) : _channel = channel;

  Map<String, dynamic> _mapFromLineItem(EvergageLineItem lineItem) {
    return {
      'attributedTerm': lineItem.attributedTerm,
      'quantity': lineItem.quantity,
      ..._mapFromProduct(lineItem.product)
    };
  }

  Map<String, dynamic> _mapFromProduct(EvergageProduct product, [String? actionName]) {
    return {
      'actionName': actionName,
      'id': product.id,
      'description': product.description,
      'imageUrl': product.imageUrl,
      'name': product.name,
      'price': product.price,
      'currency': product.currency,
      'categories': product.categories?.fold('', (prev, EvergageCategory el) => '$prev${el.id}::'),
      'tags': product.tags
          ?.fold('', (prev, EvergageTag el) => '$prev${el.id}--${describeEnum(el.tagType)}::')
    };
  }

  Future<void> addToCart(EvergageLineItem lineItem) async {
    Map<String, dynamic> lineItemMap = _mapFromLineItem(lineItem);
    if (_simulation) {
      sprint('addToCart(' + '$lineItemMap' + ')');
    } else {
      return _channel.invokeMethod('addToCart', lineItemMap);
    }
  }

  Future<void> purchase(EvergageOrder order) async {
    Map<String, dynamic> purchaseMap = {
      'orderId': order.orderId,
      'totalValue': order.totalValue,
      'totalItems': order.lineItems.length,
    };

    order.lineItems.asMap().forEach((int num, EvergageLineItem lineItem) {
      Map<String, dynamic> lineItemMap = _mapFromLineItem(lineItem);
      lineItemMap.keys.forEach((String k) {
        purchaseMap['lineItem${num + 1}-$k'] = lineItemMap[k];
      });
    });

    if (_simulation) {
      sprint('purchase(' + '$purchaseMap' + ')');
    } else {
      return _channel.invokeMethod('purchase', purchaseMap);
    }
  }

  Future<void> viewItem(EvergageProduct product, [String? actionName]) async {
    Map<String, dynamic> productMap = _mapFromProduct(product, actionName);
    if (_simulation) {
      sprint('viewItem(' + '$productMap' + ')');
    } else {
      return _channel.invokeMethod('viewItem', productMap);
    }
  }

  Future<void> viewItemDetail(EvergageProduct product, [String? actionName]) async {
    Map<String, dynamic> productMap = _mapFromProduct(product, actionName);
    if (_simulation) {
      sprint('viewItemDetail(' + '$productMap' + ')');
    } else {
      return _channel.invokeMethod('viewItemDetail', _mapFromProduct(product, actionName));
    }
  }

  // - viewCategory(Category)
}
