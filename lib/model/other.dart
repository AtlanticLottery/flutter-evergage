part of evergage;

class EvergageProduct {
  final String id;
  final double? price;
  final String currency;
  final String? description;
  final String? imageUrl;
  final String? name;
  final List<EvergageCategory>? categories;
  final List<EvergageTag>? tags;

  EvergageProduct(this.id,
      {this.currency = 'CAD',
      this.price,
      this.description,
      this.imageUrl,
      this.name,
      this.categories,
      this.tags});
}

class EvergageLineItem {
  final EvergageProduct product;

  /// Word to associate with this item that will then be used in SmartSearch.
  final String? attributedTerm;
  final int quantity;

  EvergageLineItem(this.product, {this.attributedTerm, this.quantity = 1});
}

class EvergageOrder {
  final List<EvergageLineItem> lineItems;
  final String? orderId;
  final double totalValue;

  EvergageOrder(this.lineItems, this.totalValue, {this.orderId});
}

enum EvergageTagType {
  Brand,
  Gender,
  Style,
  Author,
  Keyword,
  ItemClass,
  ContentClass,
  Size,
}

class EvergageTag {
  final String id;
  final EvergageTagType tagType;

  EvergageTag(this.id, this.tagType);
}

class EvergageCategory {
  final String id;

  EvergageCategory(this.id);
}

enum EvergageLogLevel { off, error, warn, info, debug, all }

Map<EvergageLogLevel, int> _logLevelMap = {
  EvergageLogLevel.off: 0,
  EvergageLogLevel.error: 1000,
  EvergageLogLevel.warn: 2000,
  EvergageLogLevel.info: 3000,
  EvergageLogLevel.debug: 4000,
  EvergageLogLevel.all: 2147483647,
};

class EvergageConfig {
  final String account;
  final String dataset;
  final String currency;

  EvergageConfig(this.account, this.dataset, {this.currency = 'CAD'});
}
