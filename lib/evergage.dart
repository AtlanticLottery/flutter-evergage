library evergage;

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

part 'model/context.dart';
part 'model/other.dart';

void sprint(Object s) => debugPrint('Evergage Flutter - $s');

class Evergage {
  static const MethodChannel _channel = MethodChannel('ca.alc.evergage');
  static Evergage? _instance;
  static EvergageContext? _context;

  factory Evergage() {
    assert(_instance != null, 'Must run start() first');
    return _instance!;
  }

  bool _simulation = false;

  set simulation(bool simulation) {
    if (simulation) {
      sprint('Simulation mode : ${simulation.toString().toUpperCase()} -- NOT sending data');
    } else {
      sprint('Simulation mode : ${simulation.toString().toUpperCase()} -- sending data');
    }
    _simulation = simulation;
    _context?._simulation = simulation;
  }

  /// Resets Evergage so start(EvergageConfig) can be called again with a different dataset
  Future<void> reset() async {
    if (_simulation) {
      sprint('reset()');
    } else {
      _channel.invokeMethod('reset');
    }
  }

  /// Returns the accountId, after set via setAccountId(String)
  Future<String?> getAccountId() => _channel.invokeMethod<String>('getAccountId');

  /// The user's anonymous ID, which will be used if no authenticated ID is specified for the user via setUserId(String).
  Future<String?> getAnonymousId() => _channel.invokeMethod<String>('getAnonymousId');

  /// The authenticated user's ID, once set via setUserId(String)
  Future<String?> getUserId() => _channel.invokeMethod<String>('getUserId');

  /// The optional account this user belongs to. Set this property to track which of your accounts inside the Evergage dataset and account your users belong to.
  Future<void> setAccountId(String id) async {
    if (_simulation) {
      sprint('setAccountId($id)');
    } else {
      return _channel.invokeMethod('setAccountId', {'accountId': id});
    }
  }

  /// Sets an attribute (a name/value pair) on the account. The new value will be sent to the Evergage server with the next event.
  Future<void> setAccountAttribute(String name, value) async {
    if (_simulation) {
      sprint('setAccountAttribute($name, $value)');
    } else {
      return _channel.invokeMethod('setAccountAttribute', {'name': name, 'value': value});
    }
  }
  // Future<void> setFirebaseToken(String token) =>
  //     _channel.invokeMethod('setFirebaseToken', {'token': token});

  /// Allows developers to adjust the threshold level of Evergage messages to log.
  static Future<void> setLogLevel(EvergageLogLevel logLevel) =>
      _channel.invokeMethod('setLogLevel', {'logLevel': _logLevelMap[logLevel]});

  /// Sets an attribute (a name/value pair) on the user. The new value will be sent to the Evergage server with the next event.
  Future<void> setUserAttribute(String name, value) async {
    if (_simulation) {
      sprint('setUserAttribute($name, $value)');
    } else {
      return _channel.invokeMethod('setUserAttribute', {'name': name, 'value': value});
    }
  }

  /// The authenticated user's ID. Setting this is critical to correlate the same user's activity across devices and platforms.
  Future<void> setUserId(String id) async {
    if (_simulation) {
      sprint('setUserId($id)');
    } else {
      return _channel.invokeMethod('setUserId', {'userId': id});
    }
  }

  /// Provides APIs to handle data campaigns, track Item views and interactions, and track manual actions, all within a lifecycle-managed context such as Screen.
  EvergageContext get context {
    assert(_instance != null, 'Must run start() first');
    return _context!;
  }

  /// Starts Evergage with the specified configuration. Subsequent calls to start will have no effect, unless reset().
  static Future<void> start({required EvergageConfig config}) {
    _instance = Evergage._();
    _context = EvergageContext._(_channel);
    return _channel.invokeMethod('start', {
      'accountId': config.account,
      'dataset': config.dataset,
    });
  }

  Evergage._();
}
