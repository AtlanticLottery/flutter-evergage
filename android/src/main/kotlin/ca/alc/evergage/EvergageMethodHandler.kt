package ca.alc.evergage

import android.app.Application
import android.content.Context
import com.evergage.android.ClientConfiguration
import com.evergage.android.Evergage
import com.evergage.android.Context as EvergageContext
import com.evergage.android.promote.*
import io.flutter.Log
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import org.json.JSONObject

class EvergageMethodHandler(messenger: BinaryMessenger, applicationContext: Context) :
    MethodCallHandler {

    private val channel: MethodChannel = MethodChannel(messenger, "ca.alc.evergage")
    private var application: Application? = null

    init {
        channel.setMethodCallHandler(this)
        application = applicationContext as Application
    }

    private var screen: EvergageContext? = Evergage.getInstance().globalContext

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        val needsContext = listOf("addToCart", "purchase", "viewItem", "viewItemDetail")
        if (screen?.isActive != true && needsContext.contains(call.method)) {
            return result.error("error", "Context is null! Cannot process ${call.method} -- Make sure URL Scheme is accurate", null)
        }

        val evergage = Evergage.getInstance()

        when (call.method) {
            "start" -> {
                val accountId: String = call.argument("accountId")!!
                val dataset: String = call.argument("dataset")!!

                evergage.start(
                    ClientConfiguration.Builder().account(accountId).dataset(dataset).build()
                )
                result.success(null)
            }
            "reset" -> {
                evergage.reset()
                result.success(null)
            }
            "getAccountId" -> {
                result.success(evergage.accountId)
            }
            "getAnonymousId" -> {
                result.success(evergage.anonymousId)
            }
            "getUserId" -> {
                result.success(evergage.userId)
            }
            "setAccountId" -> {
                val accountId: String = call.argument("accountId")!!
                evergage.accountId = accountId
                result.success(null)
            }
            "setAccountAttribute" -> {
                val name: String = call.argument("name")!!
                val value: String? = call.argument("value")
                evergage.setAccountAttribute(name, value)
                result.success(null)
            }
            "setFirebaseToken" -> {
                val token: String = call.argument("token")!!
                evergage.setFirebaseToken(token)
                result.success(null)
            }
            "setLogLevel" -> {
                val logLevel: Int = call.argument("logLevel")!!
                Evergage.setLogLevel(logLevel)
                result.success(null)
            }
            "setUserAttribute" -> {
                val name: String = call.argument("name")!!
                val value: String? = call.argument("value")
                evergage.setUserAttribute(name, value)
                result.success(null)
            }
            "setUserId" -> {
                val userId: String = call.argument("userId")!!
                evergage.userId = userId
                result.success(null)
            }
            "addToCart" -> {
                val product =
                    productFromJson(call.argument("id")!!, call.arguments<Map<String, Any?>>()!!)
                val lineItem = LineItem(product, call.argument("quantity"))
                lineItem.attributedTerm = call.argument("attributedTerm")
                screen!!.addToCart(lineItem)
                result.success(null)
            }
            "purchase" -> {
                val totalItems = call.argument<Double>("totalItems")!!
                val lineItems = lineItemsFromMap(totalItems, call.arguments<Map<String, Any>>()!!)
                val order = Order(call.argument("orderId"), lineItems, call.argument("totalValue"))
                screen!!.purchase(order)
                result.success(null)
            }
            "viewItem" -> {
                val product =
                    productFromJson(call.argument("id")!!, call.arguments<Map<String, Any?>>()!!)
                val actionName: String? = call.argument("actionName")
                screen!!.viewItem(product, actionName)
                result.success(null)
            }
            "viewItemDetail" -> {
                val product =
                    productFromJson(call.argument("id")!!, call.arguments<Map<String, Any?>>()!!)
                val actionName: String? = call.argument("actionName")
                screen!!.viewItemDetail(product, actionName)
                result.success(null)
            }
        }
    }

    private fun lineItemsFromMap(
        totalItems: Double,
        arguments: Map<String, Any?>
    ): MutableList<LineItem> {
        val lineItems = mutableListOf<LineItem>()
        for (i in 1..totalItems.toInt()) {
            lineItems.add(lineItems.size, lineItemFromMap(i, arguments))
        }
        return lineItems
    }

    private fun lineItemFromMap(num: Int, arguments: Map<String, Any?>): LineItem {
        val product = productFromJson(
            arguments["lineItem$num-id"] as String,
            mapOf(
                "id" to arguments["lineItem$num-id"],
                "description" to arguments["lineItem$num-description"],
                "imageUrl" to arguments["lineItem$num-imageUrl"],
                "name" to arguments["lineItem$num-name"],
                "price" to arguments["lineItem$num-price"],
                "currency" to arguments["lineItem$num-currency"],
                "categories" to arguments["lineItem$num-categories"],
                "tags" to arguments["lineItem$num-tags"]
            )
        )
        val lineItem = LineItem(product, arguments["lineItem$num-quantity"] as Int?)
        lineItem.attributedTerm = arguments["lineItem$num-attributedTerm"] as String?
        return lineItem
    }

    private fun productFromJson(id: String, arguments: Map<String, Any?>): Product {
        val product = Product.fromJSONObject(JSONObject(arguments), id)!!

        if (arguments["tags"] != null) {
            product.tags = tagsFromDelimination(arguments["tags"] as String)
        }
        if (arguments["categories"] != null) {
            product.categories = categoriesFromDelimination(arguments["categories"] as String)
        }
        Log.d("Evergage", "product: ${product.toJSONObject().toString()}")
        return product
    }

    private fun categoriesFromDelimination(category: String): List<Category> {
        return category.split("::").takeWhile { it.isNotBlank() }.map {
            Category(it)
        }
    }

    private fun tagsFromDelimination(tag: String): List<Tag> {
        return tag.split("::").takeWhile { it.isNotBlank() }.map {
            val tagSplit = it.split("--")
            Tag(tagSplit[0], tagSplit[1])
        }
    }

    fun dispose() {
    }
}
