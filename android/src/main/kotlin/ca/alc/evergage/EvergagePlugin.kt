package ca.alc.evergage

import androidx.annotation.NonNull
import io.flutter.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding

/** EvergagePlugin */
class EvergagePlugin : FlutterPlugin {
    private var methodHandler: EvergageMethodHandler? = null

    override fun onAttachedToEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodHandler = EvergageMethodHandler(binding.binaryMessenger, binding.applicationContext)
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodHandler?.dispose();
    }
}
