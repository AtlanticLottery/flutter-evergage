import 'package:flutter_evergage/evergage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  EvergageTag get tagLotto => EvergageTag('Lotto', EvergageTagType.Brand);

  Future<void> initEvergage() async {
    // config
    var config = EvergageConfig('atlanticlottery', 'abc');
    await Evergage.setLogLevel(EvergageLogLevel.all);
    await Evergage.start(config: config);
    await Future.delayed(Duration(milliseconds: 3000));

    Evergage().setUserId('foo').then((_) async {
      print(await Evergage().getUserId());
    });

    var product = EvergageProduct('Lotto649', price: 2.99, description: 'Foo Item');

    Evergage().context.viewItem(product, 'View Lotto');
    Evergage().context.viewItemDetail(product);

    var a49Ticket = EvergageProduct('Atlantic49',
        price: 3.99, tags: [tagLotto], categories: [EvergageCategory('Lotto')]);
    var lineItem = EvergageLineItem(a49Ticket, quantity: 2);
    Evergage().context.addToCart(lineItem);

    var order = EvergageOrder([lineItem], (lineItem.quantity * lineItem.product.price!));
    Evergage().context.purchase(order);
  }

  @override
  void initState() {
    super.initState();
    initEvergage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text(''),
        ),
        body: Container(),
      ),
    );
  }
}
