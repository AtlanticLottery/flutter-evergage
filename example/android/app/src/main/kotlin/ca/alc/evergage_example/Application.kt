package ca.alc.evergage_example

import com.evergage.android.Evergage
import io.flutter.app.FlutterApplication

class Application : FlutterApplication() {
    override fun onCreate() {
        Evergage.initialize(this)

        super.onCreate()
    }
}