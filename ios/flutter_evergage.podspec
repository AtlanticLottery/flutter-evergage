#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint evergage.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_evergage'
  s.version          = '3.0.2'
  s.summary          = 'Evergage SDK Flutter integration for iOS and Android.'
  s.description      = <<-DESC
Evergage SDK Flutter integration for iOS and Android.
                       DESC
  s.homepage         = 'https://www.alc.ca'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Atlantic Lottery Corp' => 'info@alc.ca' }
  s.source           = { :path => '.' }
  s.public_header_files = 'Classes/**/*.h'
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '13.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
  # Must set static_framework to TRUE when specifying a dependency
  s.static_framework = true
  s.dependency 'Evergage', '1.3.2'
end
