import Evergage
import Flutter
import UIKit

public class SwiftEvergagePlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "ca.alc.evergage", binaryMessenger: registrar.messenger())
    let instance = SwiftEvergagePlugin()

    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    var args : Dictionary<String, Any> = [:]
    if (call.arguments != nil) {
      args = call.arguments as! Dictionary<String, Any?>
    }

    let evergage = Evergage.sharedInstance()

    switch call.method {
    case "start":
      let accountId = args["accountId"] as! String
      let dataset = args["dataset"] as! String

      evergage.start { (clientConfigurationBuilder) in
        clientConfigurationBuilder.account = accountId
        clientConfigurationBuilder.dataset = dataset
      }
      return result(nil)
    case "reset":
      evergage.reset()
      return result(nil)
    case "getAccountId":
      return result(evergage.accountId)
    case "getAnonymousId":
      return result(evergage.anonymousId)
    case "getUserId":
      return result(evergage.userId)
    case "setAccountId":
      evergage.accountId = args["accountId"] as? String
      return result(nil)
    case "setAccountAttribute":
      let name = args["name"] as! String
      let value = args["value"] as? String
      evergage.setAccountAttribute(value, forName: name)
      return result(nil)
    case "setFirebaseToken":
      return result(nil)
    case "setLogLevel":
      let logLevel = args["logLevel"] as! Int
      evergage.logLevel = EVGLogLevel.init(rawValue: logLevel)!
      return result(nil)
    case "setUserAttribute":
      let name = args["name"] as! String
      let value = args["value"] as? String
      evergage.setUserAttribute(value, forName: name)
      return result(nil)
    case "setUserId":
      evergage.userId = args["userId"] as? String
      return result(nil)
    case "addToCart":
      let product = productFromJson(args)
      let lineItem = EVGLineItem(item: product, quantity: NSNumber.init(integerLiteral: args["quantity"] as! Int))
      evergage.globalContext?.add(toCart: lineItem)
      return result(nil)
    case "purchase":
      let totalItems = args["totalItems"] as! Int
      let lineItems = lineItemsFromMap(totalItems, args)
      let order = EVGOrder(id: args["orderId"] as? String, lineItems: lineItems, totalValue: NSNumber.init(floatLiteral: args["totalValue"] as! Double))
      evergage.globalContext?.purchase(order)
      return result(nil)
    case "viewItem":
      let product = productFromJson(args)
      let actionName = args["actionName"] as? String
      evergage.globalContext?.viewItem(product, actionName: actionName)
      return result(nil)
    case "viewItemDetail":
      let product = productFromJson(args)
      let actionName = args["actionName"] as? String
      evergage.globalContext?.viewItemDetail(product, actionName: actionName)
      return result(nil)
    default:
      return (result(FlutterMethodNotImplemented))
    }
  }

  private func lineItemsFromMap(_ totalItems: Int, _ arguments: Dictionary<String, Any>) -> [EVGLineItem] {
    var lineItems: [EVGLineItem] = []
    for i in 1...Int(totalItems) {
      lineItems.insert(lineItemFromMap(i, arguments), at: lineItems.count)
    }
    return lineItems
  }

  private func productFromMap(_ i: Int, _ arguments: [String: Any]) -> Dictionary<String, Any> {
    let prependLine = i == 0 ? "" : "lineItem\(i)-"
    return [
      "id": arguments["\(prependLine)id"] as Any,
      "description": arguments["\(prependLine)description"] as Any,
      "imageUrl": arguments["\(prependLine)imageUrl"] as Any,
      "name": arguments["\(prependLine)name"] as Any,
      "price": arguments["\(prependLine)price"] as Any,
      "currency": arguments["\(prependLine)currency"] as Any,
      "categories": arguments["\(prependLine)categories"] as Any,
      "tags": arguments["\(prependLine)tags"] as Any,
    ]
  }

  private func lineItemFromMap(_ i: Int, _ arguments: [String: Any]) -> EVGLineItem {
    let product = productFromJson(productFromMap(i, arguments))
    let lineItem = EVGLineItem(item: product, quantity: NSNumber.init(integerLiteral: arguments["lineItem\(i)-quantity"] as! Int))
    return lineItem
  }

  private func stringFromDict(_ key: String, _ arguments: Dictionary<String, Any>) -> String? {
      if let v = arguments[key] as? String? {
        return v
      }
    return nil
  }

  private func priceFromDict(_ arguments: Dictionary<String, Any>) -> NSNumber? {
    if let v = arguments["price"] as? Double? {
      if (v != nil) {
        return NSNumber.init(floatLiteral: v!)
      }
    }
    return nil
  }

  private func productFromJson(_ arguments: Dictionary<String, Any>) -> EVGProduct {
    let product = EVGProduct.init(
        id: arguments["id"] as! String,
        name: stringFromDict("name", arguments),
        price: priceFromDict(arguments),
        url: stringFromDict("url", arguments),
        imageUrl: stringFromDict("imageUrl", arguments),
        evgDescription: stringFromDict("description", arguments)
    )
    if let tags = arguments["tags"] as? String {
      product.tags = tagsFromDelimination(tags)
    }
    if let categories = arguments["categories"] as? String {
      product.categories = categoriesFromDelimination(categories)
    }
    return product
  }

  private func categoriesFromDelimination(_ category: String) -> [EVGCategory] {
    category.split(usingRegex: "::").filter {
      !$0.isEmpty
    }.map {
      EVGCategory(id: $0)
    }
  }

  private func tagsFromDelimination(_ tag: String) -> [EVGTag] {
    tag.split(usingRegex: "::").filter {
      !$0.isEmpty
    }.map {
      let tagSplit = $0.split(usingRegex: "--")
      return EVGTag(id: tagSplit[0], type: tagTypeFromStr(tagSplit[1]))
    }
  }

  private func tagTypeFromStr(_ tagType: String) -> EVGTagType {
    switch (tagType) {
    case "author":
      return EVGTagType.author
    case "brand":
      return EVGTagType.brand
    case "contentClass":
      return EVGTagType.contentClass
    case "gender":
      return EVGTagType.gender
    case "itemClass":
      return EVGTagType.itemClass
    case "keyword":
      return EVGTagType.keyword
    case "style":
      return EVGTagType.style
    default:
      return EVGTagType.brand
    }
  }
}

extension String {
  func split(usingRegex pattern: String) -> [String] {
    //### Crashes when you pass invalid `pattern`
    let regex = try! NSRegularExpression(pattern: pattern)
    let matches = regex.matches(in: self, range: NSRange(0..<utf16.count))
    let ranges = [startIndex..<startIndex] + matches.map {
      Range($0.range, in: self)!
    } + [endIndex..<endIndex]
    return (0...matches.count).map {
      String(self[ranges[$0].upperBound..<ranges[$0 + 1].lowerBound])
    }
  }
}
