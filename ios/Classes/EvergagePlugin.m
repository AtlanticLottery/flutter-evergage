#import "EvergagePlugin.h"
#if __has_include(<flutter_evergage/flutter_evergage-Swift.h>)
#import <flutter_evergage/flutter_evergage-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_evergage-Swift.h"
#endif

@implementation EvergagePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftEvergagePlugin registerWithRegistrar:registrar];
}
@end
